﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Decoder
{
    public class Vigenere
    {
        public string KeyWord 
        {
            get => keyWord; 
            set 
            {
                if (string.IsNullOrEmpty(value))
                {
                    keyWord = value;
                }
                else
                {
                    foreach (var item in value)
                    {
                        if (!alphabet.Contains(item))
                        {
                            throw new ArgumentException("Ключевое слово должно состоять из букв кодируемого алфавита.");
                        }
                    }
                    keyWord = value;
                }
                
                
            } 
        }
        public List<char> Alphabet { get => alphabet; set => alphabet = value; }

        private List<char> alphabet = new List<char> { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };
        private string keyWord = "скорпион";

        public Vigenere()
        {
        }
        public Vigenere(string keyWord)
        {
            KeyWord = keyWord;
        }

        public string Encoder(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }
            if (string.IsNullOrEmpty(KeyWord))
            {
                return text;
            }

            string rezult = "";
            int j = 0;

            for (int i = 0; i < text.Length; i++)
            {
                if (alphabet.Contains(char.ToLower(text[i])))
                {
                    int indexKey = alphabet.IndexOf(char.ToLower(KeyWord[j]));
                    int indexText = alphabet.IndexOf(char.ToLower(text[i]));
                    if (char.IsLower(text[i]))
                    {
                        rezult += alphabet[(indexKey + indexText) % alphabet.Count];
                    }
                    else
                    {
                        rezult += char.ToUpper(alphabet[(indexKey + indexText) % alphabet.Count]);
                    }
                    j++;
                }
                else
                {
                    rezult += text[i];
                }
                if (j >= KeyWord.Length)
                {
                    j = 0;
                }
            }
            return rezult;
        }

        public string Decoder(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }
            if (string.IsNullOrEmpty(KeyWord))
            {
                return text;
            }

            string rezult = "";
            int j = 0;

            for (int i = 0; i < text.Length; i++)
            {
                if (alphabet.Contains(char.ToLower(text[i])))
                {
                    int indexKey = alphabet.IndexOf(char.ToLower(KeyWord[j]));
                    int indexText = alphabet.IndexOf(char.ToLower(text[i]));
                    if (char.IsLower(text[i]))
                    {
                        rezult += alphabet[(indexText - indexKey + alphabet.Count) % alphabet.Count];
                    }
                    else
                    {
                        rezult += char.ToUpper(alphabet[(indexText - indexKey + alphabet.Count) % alphabet.Count]);
                    }
                    j++;
                }
                else
                {
                    rezult += text[i];
                }

                if (j >= KeyWord.Length)
                {
                    j = 0;
                }

            }
            return rezult;
        }
    }
}
