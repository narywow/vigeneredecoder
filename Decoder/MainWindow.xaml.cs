﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Decoder
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TextRange inputText;
        TextRange outputText;
        Vigenere vigenere = new Vigenere();
        public MainWindow()
        {
            InitializeComponent();
            keyWordTextBox.Text = vigenere.KeyWord;
            inputText = new TextRange(inputRTB.Document.ContentStart, inputRTB.Document.ContentEnd);
            outputText = new TextRange(outputTextViewer.Document.ContentStart, outputTextViewer.Document.ContentEnd);
        }

        private void menuOpen_Click(object sender, RoutedEventArgs e)
        {

            if (OpenFile())
            {
                UpdateAllText();
            }
        }
        private void menuSafe_Click(object sender, RoutedEventArgs e)
        {
            if (SaveFile())
            {
                MessageBox.Show("Файл успешно сохранен!");
            }
        }

        private void menuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void keyWordTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!vigenere.Alphabet.Contains(char.ToLower(e.Text[0]))) e.Handled = true;
        }
        private void keyWordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            vigenere.KeyWord = ((TextBox)sender).Text;
            UpdateAllText();
        }


        

        private void decoderRadioButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateAllText();
        }

        private void encoderRadioButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateAllText();
        }
        private void inputRTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateAllText();
        }


        private bool SaveFile()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Текстовый файл (*.txt)|*.txt|Текстовый файл (*.docx)|*.docx";

            if (saveFileDialog.ShowDialog() == true)
            {
                if (saveFileDialog.FilterIndex == 1)
                {
                    using (FileStream file = (FileStream)saveFileDialog.OpenFile())
                    {
                        using (StreamWriter stream = new StreamWriter(file, Encoding.GetEncoding(1251)))
                        {
                            stream.Write(outputText.Text);
                        }
                    }
                }
                if (saveFileDialog.FilterIndex == 2)
                {
                    object nullobj = System.Reflection.Missing.Value;
                    object file = saveFileDialog.FileName;

                    Microsoft.Office.Interop.Word.Application wordApp = null;
                    Microsoft.Office.Interop.Word.Document doc = null;
                    try
                    {
                        wordApp = new Microsoft.Office.Interop.Word.Application();
                        wordApp.Visible = false;
                        wordApp.ShowAnimation = false;

                        if (outputText.CanSave(DataFormats.Rtf))
                        {
                            string path = Directory.GetCurrentDirectory() + @"\temp.rtf";
                            using (FileStream fileStream = new FileStream(path, FileMode.Create))
                            {
                                outputText.Save(fileStream, DataFormats.Rtf);
                            }
                            doc = wordApp.Documents.Open(path);

                            doc.SaveAs(ref file, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatDocumentDefault);

                            File.Delete(path);
                        }
                        else
                        {
                            doc = wordApp.Documents.Add(ref nullobj, ref nullobj, ref nullobj, ref nullobj);
                            doc.Content.SetRange(0, 0);
                            doc.Content.Text = outputText.Text;
                            doc.SaveAs2(ref file);

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return false;
                    }
                    finally
                    {
                        doc.Close(ref nullobj, ref nullobj, ref nullobj);
                        wordApp.Quit(ref nullobj, ref nullobj, ref nullobj);
                    }
                }
                return true;
            }
            return false;
        }
        private bool OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовый файл (*.txt)|*.txt|Текстовый файл (*.docx)|*.docx";
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FilterIndex == 1)
                {
                    using (FileStream file = (FileStream)openFileDialog.OpenFile())
                    {
                        using (StreamReader stream = new StreamReader(file, Encoding.GetEncoding(1251)))
                        {
                            inputText.Text = stream.ReadToEnd();
                        }
                    }
                }
                if (openFileDialog.FilterIndex == 2)
                {
                    Microsoft.Office.Interop.Word.Application wordApp = null;
                    Microsoft.Office.Interop.Word.Document doc = null;
                    object file = openFileDialog.FileName;
                    object trueObj = true;
                    object nullobj = System.Reflection.Missing.Value;


                    try
                    {
                        wordApp = new Microsoft.Office.Interop.Word.Application();
                        wordApp.ShowAnimation = false;
                        wordApp.Visible = false;

                        doc = wordApp.Documents.Open(
                            ref file, ref nullobj, ref trueObj,
                            ref nullobj, ref nullobj, ref nullobj,
                            ref nullobj, ref nullobj, ref nullobj,
                            ref nullobj, ref nullobj, ref nullobj);
                        doc.ActiveWindow.Selection.WholeStory();
                        doc.ActiveWindow.Selection.Copy();

                        IDataObject data = Clipboard.GetDataObject();

                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (StreamWriter writer = new StreamWriter(memoryStream))
                            {
                                writer.Write(data.GetData(DataFormats.Rtf).ToString());
                                writer.Flush();
                                inputText.Load(memoryStream, DataFormats.Rtf);
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        MessageBox.Show("Ошибка открытия файла!\n" + e.Message);
                        return false;
                    }
                    finally
                    {
                        doc.Close(ref nullobj, ref nullobj, ref nullobj);
                        wordApp.Quit(ref nullobj, ref nullobj, ref nullobj);
                    }                 
                }
                return true;
            }
            return false;
        }

        public void UpdateAllText()
        {

            if (encoderRadioButton.IsChecked == true)
            {
                outputText.Text = vigenere.Encoder(inputText.Text);
            }
            if (decoderRadioButton.IsChecked == true)
            {
                outputText.Text = vigenere.Decoder(inputText.Text);
            }
            try
            {
                outputText.ApplyPropertyValue(TextElement.FontFamilyProperty, inputText.GetPropertyValue(TextElement.FontFamilyProperty));
                outputText.ApplyPropertyValue(TextElement.FontSizeProperty, inputText.GetPropertyValue(TextElement.FontSizeProperty));
                outputText.ApplyPropertyValue(TextElement.FontStyleProperty, inputText.GetPropertyValue(TextElement.FontStyleProperty));
                outputText.ApplyPropertyValue(TextElement.FontStretchProperty, inputText.GetPropertyValue(TextElement.FontStretchProperty));
                outputText.ApplyPropertyValue(TextElement.FontWeightProperty, inputText.GetPropertyValue(TextElement.FontWeightProperty));
            }
            catch (Exception e)
            {
                MessageBox.Show("Форматирование не поддерживается\n" + e.Message);
            }

        }
    }
}
