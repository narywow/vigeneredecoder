using Microsoft.VisualStudio.TestTools.UnitTesting;
using Decoder;
using System;

namespace Decoder.Tests
{
    [TestClass]
    public class VigenereTests
    {
        [TestMethod]
        public void Decoder_InputIsNull_EmptyReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder(null);

            Assert.AreEqual(rezult, "");
        }

        [TestMethod]
        public void Decoder_InputIsEmpty_EmptyReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder("");

            Assert.AreEqual(rezult, "");
        }

        [TestMethod]
        public void Decoder_Input_DecodeInputReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder("����������, ��");

            Assert.AreEqual(rezult, "����������, ��");
        }
        [TestMethod]
        public void Decoder_InputIsNotAnAlphabetLetter_InputReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder("test12_ ;");

            Assert.AreEqual(rezult, "test12_ ;");
        }
        [TestMethod]
        public void Decoder_KeyIsNull_InputReturned()
        {
            Vigenere vigenere = new Vigenere(null);
            string rezult = vigenere.Decoder("���� ");

            Assert.AreEqual(rezult, "���� ");
        }
        [TestMethod]
        public void Decoder_KeyIsEmpty_InputReturned()
        {
            Vigenere vigenere = new Vigenere("");
            string rezult = vigenere.Decoder("���� ");

            Assert.AreEqual(rezult, "���� ");
        }
        [TestMethod]
        public void Encoder_InputIsNull_EmptyReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder(null);

            Assert.AreEqual(rezult, "");
        }

        [TestMethod]
        public void Encoder_InputIsEmpty_EmptyReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Decoder("");

            Assert.AreEqual(rezult, "");
        }

        [TestMethod]
        public void Encoder_Input_DecodeInputReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Encoder("����������, ��");

            Assert.AreEqual(rezult, "����������, ��");
        }
        [TestMethod]
        public void Encoder_NotAnAlphabetLetter_InputReturned()
        {
            Vigenere vigenere = new Vigenere();
            string rezult = vigenere.Encoder("test12_ ;");

            Assert.AreEqual(rezult, "test12_ ;");
        }
        [TestMethod]
        public void Encoder_KeyIsNull_InputReturned()
        {
            Vigenere vigenere = new Vigenere(null);
            string rezult = vigenere.Encoder("���� ");

            Assert.AreEqual(rezult, "���� ");
        }
        [TestMethod]
        public void Encoder_KeyIsEmpty_InputReturned()
        {
            Vigenere vigenere = new Vigenere("");
            string rezult = vigenere.Encoder("���� ");

            Assert.AreEqual(rezult, "���� ");
        }

    }
}
